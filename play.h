#ifndef PLAY_H
#define PLAY_H
#include <QObject>
#include "carreau.h"
#include "direction.h"
#include <QObject>
#include <string>
#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

/*class reponsable à la gestion du frid du nombreux et tout les opérations de l'utilisateur */

class play: public QObject
{
    Q_OBJECT
public:

    explicit play(int lig,int col);
    play(const play& n);
    ~play();
    //getters et setters
    carreau** getGrid();
    int getLigne();
    int getColonne();
    int get_nb_cases();
    QString getScore();
    bool getEnded();
    void setGrid(carreau** c);

    play& operator=(const play& c);
    Q_INVOKABLE void start();
    Q_INVOKABLE void sumGrid(direction d);
    Q_INVOKABLE void sumGridUp();
    Q_INVOKABLE void sumGridDown();
    Q_INVOKABLE void sumGridRight();
    Q_INVOKABLE void sumGridLeft();
    Q_INVOKABLE void restorePrec();

    void update();
    bool verifEtatPrecMax();
    void updateAnnul();
    void addGridToPrec();
    bool verifWin();
    bool verifBlock(direction d,carreau** grid);
    void addscore(int);
    void setScore(int);
    QString getBestScore();
    bool verifTot();

    Q_INVOKABLE void new_game();
    Q_INVOKABLE QString flatenGrid();
    Q_INVOKABLE void set_grille(int lignes,int colonnes);

    Q_PROPERTY(int ligne READ getLigne NOTIFY changed());
    Q_PROPERTY(int colonne READ getColonne NOTIFY changed());
    Q_PROPERTY(int nb_cases READ get_nb_cases NOTIFY changed)

    Q_PROPERTY(carreau** grid READ getGrid NOTIFY changed());
    Q_PROPERTY(bool ended READ getEnded NOTIFY changed());
    Q_PROPERTY(bool ended READ getEnded NOTIFY changed());
    Q_PROPERTY(QString valeur READ flatenGrid NOTIFY changed());
    Q_PROPERTY(QString score READ getScore NOTIFY modifScore());
    Q_PROPERTY(QString best_score READ getBestScore NOTIFY modifScore());

signals:
    void changed();
    void modifScore();

private:
   int ligne=4;
   int* scoretab; // tableau des scores
   int colonne=4;
   bool ended;// la partie est terminé ou non
   int compteur=0;
   int score=0;
   int best_score=0;
   int nbEtatStocke=0; //nombre des etats stockés
   int nbEtatStockeMax=2000; //nombre des etats precedents à stocker
   QString valeur;
   carreau*** etatprec; //cube des etats précédentes de la grille
   carreau** grid; // la grille
   void alloc (int l,int c); //allouer la grille
   void free();//désallocation de la grille
   bool endTab[4]={false,false,false,false}; // tableau pour verifier si la grille reste la meme dans tous les directions de sommation
   bool blocked=false;
};

#endif // PLAY_H
