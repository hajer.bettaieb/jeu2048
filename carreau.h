#ifndef CARREAU_H
#define CARREAU_H

#include <QObject>

class carreau: QObject
{
    Q_OBJECT
public:
    //constructeur par defaut+constructeur parametré+constructeur de recopie
    explicit carreau();
    explicit carreau(int);
    carreau(const carreau& c);
    void setValue(QString);
    carreau& operator=(const carreau& c);
    QString getValue();
private:
    QString value;

};

#endif // CARREAU_H
