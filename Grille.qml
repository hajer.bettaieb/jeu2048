import QtQuick.Window 2.2
import QtQml.Models 2.2
import QtQml
import QtQuick 2.15

Rectangle {

    id: board
    property int gridSpacing: 8

    width:360
    height:300
    color: "#b2aaaa"
    radius: 7
    Grid {
        id: grid
        rows: 4
        columns: 4
        spacing: 8


        width: board.width - (1+columns)*spacing
        height: board.height - (1+rows)*spacing
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.leftMargin: spacing
        anchors.topMargin: spacing
        Component.onCompleted: forceActiveFocus()
       
        Keys.onPressed: {
          switch (event.key) {
            case Qt.Key_Up:
              ng.sumGridUp();
              break;
            case Qt.Key_Down:
              ng.sumGridDown();
              break;
            case Qt.Key_Right:
              ng.sumGridRight();
              break;
            case Qt.Key_Left:
              ng.sumGridLeft();
              break;
          }
        }
        Repeater {
                id: tiles

                model: ng.nb_cases
                delegate: Carreau{
                    focus:true
                    width: Math.round(grid.width / grid.columns)
                    height: Math.round(grid.height / grid.rows)


                }




            }
}
}











