import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQml.Models 2.15

//C'est la premiere page qui s'affiche qui permet l utilisateur de choisir la taille de la grille
Window {
    width: 450
    height: 480
    maximumHeight: height
    maximumWidth: width

    minimumHeight: height
    minimumWidth: width
    visible: true


    Item {
        width: 470
        Rectangle {
                    id: rectangle1
                    x: 20
                    y: 40
                    width: 400
                    height: 300
                    color: "#A9957B"

                    Text {
                        id: textStart
                        x: 100
                        y: 40
                        width: 57
                        height: 19
                        text: qsTr("Les régles du jeu 2048 ")
                        color:"#4080B7"
                        font.pixelSize: 20
                        font.styleName: "Gras"
                        font.family: "Serif"

                        font.bold: true
                    }
                    Text {
                        id: textPlay
                        x: 30
                        y: 100
                        width: 57
                        height: 19
                        text: qsTr("Sur une grille de 16 cases, on fait bouger
des nombres multiples et puissances
de 2 à l'aide des flèches directionnelles
du clavier.Le but est d'additionner ceux
de même valeur,2+2, 4+4, 8+8, pour
atteindre 2048 sur une case,sans que
la grille ne soit bloquée.")
                        font.pixelSize: 15
                        color:"#2F1B0C"
                        font.family: "Verdana"

                        font.bold: true
                    }



        }}


    Image {
        id: button
        x: 180
        y: 400
        width: 90
        height: 40
        source: "images/playsat.jpg"
        MouseArea{
            x: 0
            y: 0
            width: 80
            height: 63
            onClicked: rpmodel.append({})

                ListModel {
                    id: rpmodel
                }

                Instantiator {
                    model: rpmodel
                    delegate: Window {
                        width: 480
                        height: 650
                        maximumHeight: height
                        maximumWidth: width

                        minimumHeight: height
                        minimumWidth: width
                        visible: true

                        PageJeu{
                            id:pagejeu
                        }


                        Item{
                            Loader{
                                id: loadBoard
                                source:"Grille.qml"
                                x:60
                                y:270
                                function reload(nb_rows, nb_cols) {
                                    source = "";
                                    grille.reset(nb_rows, nb_cols, win_value);
                                    source = "Grille.qml";


                                }
                            }
                        }

                        onClosing: rpmodel.remove(index)
                    }
                }
        }

    }
}

/*##^##
Designer {
    D{i:0;formeditorZoom:0.75}D{i:1}
}
##^##*/

