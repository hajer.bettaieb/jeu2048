#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "play.h"
#include <QQmlContext>
#include <cstdlib>
#include <ctime>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    play grid=play(4,4);

    srand(time(NULL));
    int v= (rand()%4)+1;

    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.rootContext()->setContextProperty("ng",&grid);
    engine.load(url);

    return app.exec();
}
