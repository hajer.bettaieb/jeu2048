#include "play.h"
#include <fstream>
#include <iostream>
#include <sstream>
using namespace std;

//allocation de la grille


void play::alloc(int l, int c){
    ligne = l;
    colonne = c;
    grid = new carreau*[ligne];

    for(int i=0; i<ligne; i++)
    {
        grid[i] = new carreau[colonne];
    }
    scoretab=new int[nbEtatStockeMax];
    for(int p=0;p<nbEtatStockeMax;p++)
    {
        scoretab[p]=0;
    }
    etatprec=new carreau**[nbEtatStockeMax];
    for (int i=0;i<nbEtatStockeMax;i++)
    {
        etatprec[i]=new carreau*[ligne];
        for (int j=0;j<ligne;j++)
        {
            etatprec[i][j]=new carreau[colonne];
        }
    }
    for (int i=0;i<nbEtatStockeMax;i++)
    {
        for (int k=0;k<ligne;k++)
        {
            for (int j=0;j<colonne;j++)
            {
                etatprec[i][k][j].setValue(" ");
            }
        }
    }

}

//constructeur

play::play(int lig,int col)
{
    alloc(lig,col);
}

//constructeur de recopie


play::play(const play& c)
{   this->nbEtatStockeMax=c.nbEtatStockeMax;
    this->nbEtatStocke=c.nbEtatStocke;
    this->compteur=c.compteur;
    this->ended=c.ended;
    this->ligne=c.ligne;
    this->colonne=c.colonne;
    this->grid=new carreau*[this->ligne];
    for (int i=0;i<this->ligne;i++)
       {
        this->grid[i]=new carreau[colonne];
       }
    for (int i=0;i<this->ligne;i++)
    {
        for (int j=0;j<this->colonne;j++)
        {
            this->grid[i][j]=c.grid[i][j];
        }
    }

    etatprec=new carreau**[nbEtatStockeMax];
    for (int i=0;i<nbEtatStockeMax;i++)
    {
        etatprec[i]=new carreau*[ligne];
        for (int j=0;j<ligne;j++)
        {
            etatprec[i][j]=new carreau[colonne];
        }
    }
    scoretab= new int[nbEtatStockeMax];
    for (int p=0 ; p<nbEtatStockeMax;p++)
    {
        scoretab[p]=c.scoretab[p];
    }
    for (int i=0;i<nbEtatStockeMax;i++)
    {
        for (int k=0;k<ligne;k++)
        {
            for (int j=0;j<colonne;j++)
            {
                etatprec[i][k][j]=c.etatprec[i][k][j];
            }
        }
    }
}
//gettes et setters

void play::setGrid(carreau** c)
{
    this->grid=c;
}

carreau** play::getGrid()
{
    return this->grid;
}
int play::getColonne(){
    return this->colonne;
}
int play::getLigne()
{
    return this->ligne;
}
int play::get_nb_cases(){
    return ligne*colonne;
}
QString play::getScore()
{
    return QString::number(score);

}
//operateur d'affectation


play& play::operator=(const play& c)
{   this->nbEtatStocke=c.nbEtatStocke;
    this->nbEtatStockeMax=c.nbEtatStockeMax;
    this->compteur=c.compteur;
    this->ended=c.ended;
    this->ligne=c.ligne;
    this->colonne=c.colonne;
    this->grid=new carreau*[this->ligne];
    for (int i=0;i<this->ligne;i++)
       {
        this->grid[i]=new carreau[colonne];
       }
    for (int i=0;i<this->ligne;i++)
    {
        for (int j=0;j<this->colonne;j++)
        {
            this->grid[i][j]=c.grid[i][j];
        }
    }
    scoretab=new int[nbEtatStockeMax];
    for (int p=0;p<nbEtatStockeMax;p++)
    {
        scoretab[p]=c.scoretab[p];
    }
    etatprec=new carreau**[nbEtatStockeMax];
    for (int i=0;i<nbEtatStockeMax;i++)
    {
        etatprec[i]=new carreau*[ligne];
        for (int j=0;j<ligne;j++)
        {
            etatprec[i][j]=new carreau[colonne];
        }
    }
    for (int i=0;i<nbEtatStockeMax;i++)
    {
        for (int k=0;k<ligne;k++)
        {
            for (int j=0;j<colonne;j++)
            {
                this->etatprec[i][k][j]=c.etatprec[i][k][j];
            }
        }
    }
    return *this;
}

//affichage de la grille

void play::set_grille(int lignes, int colonnes){
    free();
    alloc(lignes,colonnes);
    emit changed();

}

//sauvegarde de l'etat precedent


void play::addGridToPrec()
{
    for(int i=0;i<ligne;i++)
    {
        for (int j=0;j<colonne;j++)
        {
            etatprec[nbEtatStocke][i][j]=grid[i][j];
        }
    }
    addscore(getScore().toInt());
    nbEtatStocke++;
}



//initialisation de la grille


void play::start()
{   srand(time(NULL));
    bool ok = false;
    int pos1_i=rand()%ligne;
    int pos1_j=rand()%ligne;
    int pos2_i,pos2_j;
    while (!ok)
    {
        pos2_i= rand()%ligne;
        pos2_j= rand()%ligne;

        if (pos1_i!=pos2_i || pos2_j!=pos1_j)
        {
         ok = true;
        }
    }
    grid[pos1_i][pos1_j]=*(new carreau(0));
    grid[pos2_i][pos2_j]=*(new carreau(0));
   compteur=0;
   emit changed();
   for (int h=0;h<4;h++)
   {
       endTab[h]=false;
   }

}

// Update grille


void play::update()
{

    int ligne=this->getLigne();
    int colonne =this->getColonne();

    int lin=rand()%ligne;
    int col=rand()%colonne;
    while(this->getGrid()[lin][col].getValue()!=" ")
    {
        lin=rand()%ligne;
        col=rand()%colonne;
    }
    this->getGrid()[lin][col]=*(new carreau(0));
}

//lire a partir du fichier le meilleur score
QString play::getBestScore(){
    fstream fichier;
    string ligne;
    fichier.open("bestScore", ios::in);
    fichier >>ligne;
    int best_actuel;
    // convertir en un int
    best_actuel= atoi(ligne.c_str());
        if(score>best_actuel){
              best_score=score;

        }
    return QString::number(best_score);
}

// Recherche d une case vide

int* firstEmpty(play g,direction d , int n, int m )
{   int* tab = new int[3];
    tab[0]=0;

    switch(d)
    {
        case 1:
         for (int i=n;i>-1;i--)
         {

             if (g.getGrid()[i][m].getValue()==" ")
             {
                 tab[0]=1;
                 tab[1]=i;
                 tab[2]=m;

             }

         }
         return tab;
        case 2:
        for (int i=n;i<g.getLigne();i++)
        {
            if (g.getGrid()[i][m].getValue()==" ")
            {
                tab[0]=1;
                tab[1]=i;
                tab[2]=m;

            }

        }
        return tab;
        case 3:
        for (int i=m;i<g.getColonne();i++)
        {
            if (g.getGrid()[n][i].getValue()==" ")
            {
                tab[0]=1;
                tab[1]=n;
                tab[2]=i;

            }

        }
        return tab;
        case 4:
        for (int i=m;i>-1;i--)
        {
            if (g.getGrid()[n][i].getValue()==" ")
            {
                tab[0]=1;
                tab[1]=n;
                tab[2]=i;

            }

        }
        return tab;
    }
}

//verifSum

bool verifsum(carreau a , carreau b)
{
    return a.getValue()==b.getValue();
}


bool** initverif(int ligne,int colonne)
{
    bool** verif;
    verif = new bool*[ligne];
    for (int i=0;i<ligne;i++)
       {
        verif[i]=new bool[colonne];

       }
    for(int i=0;i<ligne;i++)
    {
        for (int j=0;j<colonne;j++)
        {
            verif[i][j]=false;
        }
    }
   return verif;
}

// verifification de nbre d etat max n est pas depasse

bool play::verifEtatPrecMax()
{
    if(nbEtatStocke<nbEtatStockeMax)
    {
        return true;
    }
    else
    {
        return false;
    }
}
//
//modification de la grille aprés la modification

void play::updateAnnul()
{   carreau*** etatprecc;
    etatprecc=new carreau**[nbEtatStockeMax];
    for (int i=0;i<nbEtatStockeMax;i++)
    {
        etatprecc[i]=new carreau*[ligne];
        for (int j=0;j<ligne;j++)
        {
            etatprecc[i][j]=new carreau[colonne];
        }
    }
    for (int i=0;i<nbEtatStockeMax;i++)
    {
        for (int k=0;k<ligne;k++)
        {
            for (int j=0;j<colonne;j++)
            {
                etatprecc[i][k][j]=this->etatprec[i][k][j];
            }
        }
    }

    etatprec=new carreau**[nbEtatStockeMax*2];
    for (int i=0;i<nbEtatStockeMax;i++)
    {
        etatprec[i]=new carreau*[ligne];
        for (int j=0;j<ligne;j++)
        {
            etatprec[i][j]=new carreau[colonne];
        }
    }
    for (int i=0;i<nbEtatStockeMax;i++)
    {
        for (int k=0;k<ligne;k++)
        {
            for (int j=0;j<colonne;j++)
            {
                etatprec[i][k][j]=etatprecc[i][k][j];
            }
        }
    }
    int* abs=new int[nbEtatStockeMax];
    for (int p=0;p<nbEtatStockeMax;p++)
    {
        abs[p]=scoretab[p];
    }
    scoretab=new int[nbEtatStockeMax*2];
    for (int p=0;p<nbEtatStockeMax;p++)
    {
        scoretab[p]=abs[p];
    }
    this->nbEtatStockeMax=nbEtatStockeMax*2;
}


void play::addscore(int a)
{
  scoretab[nbEtatStocke]=a;
}

//verification d une case vide pour avoir si le jeu est terminé

bool caseVide(carreau** grid,int ligne,int colonne)
{
    for (int i=0;i<ligne;i++)
    {
        for (int j=0;j<colonne;j++)
        {
            if(grid[i][j].getValue()==" ")
            {
                return true;
            }
        }
    }
    return false;
}


//sommation des case de meme direction


void play::sumGrid(direction d)
{   bool** gridverif=initverif(ligne,colonne);

    if(verifEtatPrecMax()){
    addGridToPrec();

    }
    else
    {
        updateAnnul();
        addGridToPrec();


    }
    switch(d)
    {
    int* coord;
    case 1:
        for (int i=0;i<ligne;i++)
        {
            for (int j=0;j<colonne;j++)
            {

                if(grid[i][j].getValue()!=" ")
                {
                    coord= firstEmpty(*this,d,i,j);
                    if(coord[0]==0)
                    {
                        if(i!=0)
                        {
                            if(verifsum(grid[i][j],grid[i-1][j])&&(gridverif[i-1][j]==false))
                            {
                                grid[i-1][j].setValue(QString::number(grid[i-1][j].getValue().toInt()*2));
                                score=score+grid[i-1][j].getValue().toInt();
                                emit modifScore();
                                grid[i][j]=*(new carreau());
                                gridverif[i-1][j]=true;
                            }
                        }
                        else
                        {
                            cout<<"impossible"<<endl;
                        }
                    }
                    else
                    {
                        if(coord[1]==0)
                        {


                            grid[coord[1]][coord[2]]=grid[i][j];
                            grid[i][j]= *(new carreau());



                        }
                        else
                        {
                            if(verifsum(grid[coord[1]-1][coord[2]],grid[i][j])&&gridverif[coord[1]-1][coord[2]]==false)
                            {
                                grid[coord[1]-1][coord[2]].setValue(QString::number(grid[coord[1]-1][coord[2]].getValue().toInt()*2));
                                score=score+grid[coord[1]-1][coord[2]].getValue().toInt();
                                emit modifScore();
                                grid[i][j]=*(new carreau());
                                gridverif[coord[1]-1][coord[2]]=true;
                            }
                            else
                            {

                                grid[coord[1]][coord[2]]=grid[i][j];
                                grid[i][j]= *(new carreau());


                            }
                        }
                    }
                }
            }

        }
       delete[] coord;

    case 2:
        for (int i=ligne-1;i>-1;i--)
        {
            for (int j=0;j<colonne;j++)
            {
                if(grid[i][j].getValue()!=" ")
                {coord= firstEmpty(*this,d,i,j);
                    if(coord[0]==0)
                    {
                        if(i!=(ligne-1))
                        {
                            if(verifsum(grid[i+1][j],grid[i][j])&&gridverif[i+1][j]==false)
                            {
                                grid[i+1][j].setValue(QString::number(grid[i+1][j].getValue().toInt()*2));
                                score=score+grid[i+1][j].getValue().toInt();
                                emit modifScore();
                                grid[i][j]=*(new carreau());
                                gridverif[i+1][j]=true;
                            }
                        }
                        else
                        {
                            cout<<"impossible"<<endl;
                        }
                    }
                    else
                    {
                        if(coord[1]==ligne-1)
                        {


                            grid[coord[1]][coord[2]]=grid[i][j];
                            grid[i][j]= *(new carreau());

                        }
                        else
                        {
                            if(verifsum(grid[coord[1]+1][coord[2]],grid[i][j])&&gridverif[coord[1]+1][coord[2]]==false)
                        {
                            grid[coord[1]+1][coord[2]].setValue(QString::number(grid[coord[1]+1][coord[2]].getValue().toInt()*2));
                            score=score+grid[coord[1]+1][coord[2]].getValue().toInt();
                            emit modifScore();
                            grid[i][j]=*(new carreau());
                            gridverif[coord[1]+1][coord[2]]=true;
                        }
                        else
                        {

                                grid[coord[1]][coord[2]]=grid[i][j];
                                grid[i][j]= *(new carreau());


                        }
                        }
                    }
                }}

        }
       delete[] coord;
        break;
    case 3:
        for (int j=colonne-1;j>-1;j--)
        {
            for (int i=0;i<ligne;i++)
            {
                if(grid[i][j].getValue()!=" ")
                {coord= firstEmpty(*this,d,i,j);
                    if(coord[0]==0)
                    {
                        if(j!=colonne-1)
                        {
                            if(verifsum(grid[i][j+1],grid[i][j])&&gridverif[i][j+1]==false)
                            {
                                grid[i][j+1].setValue(QString::number(grid[i][j+1].getValue().toInt()*2));
                                score=score+grid[i][j+1].getValue().toInt();
                                emit modifScore();
                                grid[i][j]=*(new carreau());
                                gridverif[i][j+1]=true;
                            }
                        }
                        else
                        {
                            cout<<"impossible"<<endl;
                        }
                    }
                    else
                    {
                        if(coord[2]==colonne-1)
                        {

                            grid[coord[1]][coord[2]]=grid[i][j];
                            grid[i][j]= *(new carreau());


                        }
                        else
                        {
                            if(verifsum(grid[coord[1]][coord[2]+1],grid[i][j])&&gridverif[coord[1]][coord[2]+1]==false)
                            {
                                grid[coord[1]][coord[2]+1].setValue(QString::number(grid[coord[1]][coord[2]+1].getValue().toInt()*2));
                                score=score+grid[coord[1]][coord[2]+1].getValue().toInt();
                                emit modifScore();
                                grid[i][j]=*(new carreau());
                                gridverif[coord[1]][coord[2]+1]=true;
                            }
                            else
                            {

                                grid[coord[1]][coord[2]]=grid[i][j];
                                grid[i][j]= *(new carreau());


                            }
                        }}
                }}
}

       delete[] coord;
        break;
    case 4:
        for (int j=0;j<colonne;j++)
        {
            for (int i=0;i<ligne;i++)
            {
                if(grid[i][j].getValue()!=" ")
                {coord= firstEmpty(*this,d,i,j);
                    if(coord[0]==0)
                    {
                        if(j!=0)
                        {
                            if(verifsum(grid[i][j-1],grid[i][j])&&gridverif[i][j-1]==false)
                            {
                                grid[i][j-1].setValue(QString::number(grid[i][j-1].getValue().toInt()*2));
                                score=score+grid[i][j-1].getValue().toInt();
                                emit modifScore();
                                grid[i][j]=*(new carreau());
                                gridverif[i][j-1]=true;
                            }
                        }
                        else
                        {
                            cout<<"impossible"<<endl;
                        }
                    }
                    else
                    {
                        if(coord[2]==0)
                        {

                            grid[coord[1]][coord[2]]=grid[i][j];
                            grid[i][j]= *(new carreau());


                        }
                        else {
                            if(verifsum(grid[coord[1]][coord[2]-1],grid[i][j])&&gridverif[coord[1]][coord[2]-1]==false)
                        {
                            grid[coord[1]][coord[2]-1].setValue(QString::number(grid[coord[1]][coord[2]-1].getValue().toInt()*2));
                            score=score+grid[coord[1]][coord[2]-1].getValue().toInt();
                            emit modifScore();
                            grid[i][j]=*(new carreau());

                        }
                            else{

                                grid[coord[1]][coord[2]]=grid[i][j];
                                grid[i][j]= *(new carreau());


                            }
                    }}
                }}

        }
       delete[] coord;
       break;
    }
    delete[] gridverif;
    if(caseVide(this->grid,this->ligne,this->colonne)){
    update();
    }
    emit changed();



    verifBlock(d,etatprec[nbEtatStocke-1]);
    verifTot();

    for (int h=0;h<4;h++)
    {
       cout<<endTab[h]<<endl;
    }
    if(verifWin()){
        cout<<"gagner"<<endl;
        blocked=true;
    }

}
bool play::getEnded()
{
    return this->ended;
}

//fin du jeu

/*préparer la grid pour l'affichage  */

QString play::flatenGrid()
{   if(compteur==ligne*colonne)
    {
        compteur=0;
    }
    int k=0;
    int* tab = new int[ligne*colonne];
    for (int i=0;i<ligne;i++)
    {
        for(int j=0;j<colonne;j++)
        {
            tab[k]=grid[i][j].getValue().toInt();
            k++;
        }
    }

    if(tab[compteur]==0)
    {
        compteur++;
        delete[]tab;
        QString s=" ";
        return s;
    }
    else
    {valeur=QString::number(tab[compteur]);
        delete[]tab;
        compteur++;
    return valeur;
    }



}



/*méthode de sommation selon la direction  */


void play::sumGridUp()
{ if(!blocked){
    this->sumGrid((enum direction)(1));
    emit changed();
    }
}
void play::sumGridDown()
{   if(!blocked){
    this->sumGrid((enum direction)(2));
    emit changed();
    }
}
void play::sumGridRight()
{   if(!blocked){
    this->sumGrid((enum direction)(3));
    emit changed();
}}
void play::sumGridLeft()
{   if(!blocked){
    this->sumGrid((enum direction)(4));
    emit changed();
}}
void play::setScore(int a)
{
    score=a;
}

/*affecter l'etat précedente de la grille  */

void play::restorePrec()
{
if((nbEtatStocke!=0))
{
for(int i=0;i<ligne;i++)
{
    for(int j=0;j<colonne;j++)
    {
        grid[i][j].setValue(etatprec[nbEtatStocke-1][i][j].getValue());
    }

}
        setScore(scoretab[nbEtatStocke-1]);
        nbEtatStocke=nbEtatStocke-1;
        cout<<nbEtatStocke<<endl;
        emit changed();
        emit modifScore();
        blocked=false;

}
}

/*initialisé une nouvelle partie  */


void play::new_game(){
    set_grille(ligne,colonne);
    start();
    cout<<"let's start"<<endl;
}

/*verifier si l'utilisateur à gagné  */


bool play::verifWin()
{
    for(int i=0;i<ligne;i++)
    {
        for(int j=0;j<colonne;j++)
        {
            if(grid[i][j].getValue().toInt()==2048)
            {

                emit changed();
                return true;
            }
        }
    }
    return false;
}

/*verifier si deux etats de la grille sont somblales  */

bool semblable(carreau** grid1,carreau** grid2,int ligne,int colonne )
{
    for (int i=0;i<ligne;i++)
    {
        for (int j=0;j<colonne;j++)
        {
            if (grid1[i][j].getValue()!=grid2[i][j].getValue())
            {
                return false;
            }
        }
    }
    return true;
}

/*verifier si l'utilisateur à perdu  */


bool play::verifBlock(direction d,carreau** grid)
{
    if (nbEtatStocke!=0){
        if (semblable(this->grid,grid,this->ligne,this->colonne)){
            switch(d)
            {
            case 1 : endTab[0]=true;
            case 2 : endTab[1]=true;
            case 3 : endTab[2]=true;
            case 4 : endTab[3]=true;
            }
            return true;
        }
        else{
            for (int h=0;h<4;h++)
            {
                endTab[h]=false;
            }
           return false ;
        }
    }
    else{
        return false;
    }
}
bool play::verifTot()
{
    for (int i=0 ; i<4;i++)
    {
        if (endTab[i]==false)
        {
            return false;
        }
    }

    return true;
}


//désallocation de la grille

void play::free(){
    for (int i=0; i<ligne; i++) {
        delete [] grid[i];
    }
    delete [] grid;

    for (int j=0;j<nbEtatStockeMax;j++)
    {
        for (int i=0;i<ligne;i++)
        {
                delete[] etatprec[j][i];
        }
    }
    for (int i=0;i<nbEtatStockeMax;i++)
    {
        delete[] etatprec[i];
    }
    delete[] etatprec;
    delete[] scoretab;

}

//destructeur

play::~play()
{
    free();
}


