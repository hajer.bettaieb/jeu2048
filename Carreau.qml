import QtQuick 2.15

Rectangle {

    id: tile
    radius: 3

    function choose_indice(){

           switch(tile_value.text){

            case qsTr("2"):
                return 1
            case qsTr("4"):
                return 2
            case qsTr("8"):
                return 3
            case qsTr("16"):
                return 4
            case qsTr("32"):
                return 5
            case qsTr("64"):
                return 6
            case qsTr("128"):
                return 7
            case qsTr("256"):
                return 8
            case qsTr("512"):
                return 9
            case qsTr("1024"):
                return 10
             case qsTr("2048"):
                 return 11
             default:
                 return 0

            }
        }


    TextEdit {
        id: tile_value
        text: ng.valeur
        readOnly: true

        font.pixelSize: 25
        font.family: police
        anchors.fill: parent
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter

        font.styleName:police

    }








}
