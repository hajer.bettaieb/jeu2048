#include "carreau.h"

//generation d'un carreau qui a une valeur egal à 2 ou 4
carreau::carreau(int c)
{
    int FLOAT_MIN = 0;
    int FLOAT_MAX = 1;
    float nb = FLOAT_MIN + (float)(rand()) / ((float)(RAND_MAX/(FLOAT_MAX - FLOAT_MIN)));
    if (nb>0.5){
        this->value="4";
    }
    else{
        this->value="2";

    }

}
//pour les carreaux vide
carreau::carreau()
    {
    this->value=" ";

    }
/***********/
carreau::carreau(const carreau& c)
   {
   this->value=c.value;
    }

carreau& carreau::operator=(const carreau& c)
    {
        this->value=c.value;
        return *this;
    }

void carreau::setValue(QString chaine)
    {
        this->value=chaine;
    }

QString carreau::getValue()
    {
        return this->value;
    }
